# About

I am a Game Designer & Developer aspiring to tell gripping stories and create engaging mechanics in the games I create.  
I am a Data Scientist dedicated to understanding the underlying meaning within data and guiding business descisions with evidence based research.  
I have a passion for devising novel solutions to interesting problems.    
I have a Honours BSc in Game Design & Development and an MSc in Data Science.

 ## Skills
 
 <table><tbody>
	 <tr><td>Game Design</td><td>Game Mechanics</td><td>Game Development</td><td>Game AI</td></tr>
	 <tr><td>Maths</td><td>Algorithms</td><td>Data Structures</td><td>ECS</td></tr>
	 <tr><td>Godot</td><td>Unreal Engine</td><td>Unity</td><td>TIC 80</td></tr>
	 <tr><td>C++</td><td>C</td><td>C#</td><td>Lisp (Scheme)</td></tr>
	 <tr><td>Python</td><td>Data Analytics</td><td>Visualisation</td><td>AI</td></tr>
	 <tr><td>Git</td><td>Linux</td><td>Shell Scripting</td><td>HTML / JavaScript / CSS</td></tr>
 </tbody></table>

 ## Events
 
 <table><tbody>
	 <tr><td>WOWZAPP 2012</td><td>Cork Game Craft 2013</td></tr>
	 <tr><td>GGJ 2012</td><td>Games Fleadh 2012</td></tr>
	 <tr><td>GGJ 2013</td><td>Games Fleadh 2013</td></tr>
	 <tr><td>GGJ 2014</td><td>Games Fleadh 2014</td></tr>
	 <tr><td>GGJ 2018</td><td>Games Fleadh 2015</td></tr>
 </tbody></table>
 
